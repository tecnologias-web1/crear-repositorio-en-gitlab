## Tutorial para crear una cuenta en Gitlab y nuestro primer repositorio

### Video explicativo:


![Crear un repositorio en GitLab](crear-cuenta-y-repo-gitlab.mp4)


### Cómo crear un repositorio en Gitlab, clonarlo a mi equipo, hacer modificaciones y subirlas. 


1) Como primera medida, ingresamos al sitio <a href="https://about.gitlab.com/" target="_blank">Gitlab</a> 

2) Hacemos clic en la esquina superior derecha ***"Sign in"***. Puede que no aparezca al instante esta opción, sean pacientes :)

3) Una vez en la ventana de logueo, ya que aún no tenemos cuenta, vamos a hacer clic abajo en el texto debajo que dice: ***"Register now"***.

4) Completamos los datos, agregando una dirección de correo válida, ya que después con la misma vamos a realizar la operación de validación de la cuenta.

5) Una vez registrados, nos llegará a la casilla indicada un código aleatorio que nos envía Gitlab para poder validar los datos proporcionados y proceder con la creación de nuestro primer repositorio. 

6) Ingresamos a nuestro correo, copiamos el código recibido y posteriormente lo pegamos donde nos solicita dicho código de validación.

7) En la ventana siguiente nos ofrecerá crear o importar nuestro primer proyecto. En esta caso vamos a crearlo, permaneciendo en la primera solapa ***"Create"***.

8) Generamos un grupo con el nombre identificativo que nos guste para agrupara nuestros proyectos y debajo asignamos el nombre al primero de ellos. Tildamos además incluir **README**, el único casillero que tenemos para marcar. Presionamos en ***"Create project"***.

9) En la ventana siguiente, luego del mensaje previo de felicitación por parte de la web por crear el primer repositorio, iremos a la esquina superior izquierda, cliqueando en el nombre del repo que asignamos, debajo de la leyenda ***"Project"***.

10) Esta acción nos llevará a la ventana principal del proyecto, donde podremos leer el texto por defecto del *"README"*, archivo de texto el cual nos servirá para describir y en un futuro detallar los pasos por medio de los cuales una persona ajena al mismo podrá clonarlo y "levantarlo" en su equipo. 

**Nota aclaratoria:** nuestro proyecto por fue creado en modo "privado" por el sistema; esto es, sólo quienes posean clave p****odrán descargarlo. Para que cualquier navegante pueda acceder al repositorio libremente, será necesario poner el mismo en público. 

11) Vamos a hacer clic en el botón azul "Code", y se desplegará un menú done vamos a seleccionar "Clone with HTTPS". Esto nos copiará en memoria esta dirección.

12) Minimizamos el navegador, vamos al directorio de nuestro sistema deseado (recomiendo Documentos) y una vez allí, para los sistemas basados en Debian, botón derecho del mouse, abrir terminal aquí. Una vez dentro de la terminal,vamos a proceder a clonar el repositorio de la siguiente manera:

```
git clone nombre_del_repo_copiado
```

Donde nombre_del_repo_copiado se corresponde con la dirección que acabamos de copiar de Gitlab.

**Nota 2:** Si tenemos instalado Git, acto seguido procederá a clonarse el repo, es decir, a copiar el contenido en nuestra máquina. Para eso nos va a pedir clave, vamos a usar la que generamos al crear la cuenta en Gitlab.
Si nos menciona que no tenemos instalado git, procedemos a instalarlo y luego repetiremos el paso anterior para clonar.

```
sudo apt install git
```

13) Luego de clonar el repositorio, entramos al mismo:

cd nombre_del_repo_copiado

Vamos a editar el README.md con nuestro edito de texto favorito. El contenido del mismo está en Markdown, un lenguaje de escritura dinámico, del cual pueden interiorizarse en el <a href="https://markdown.es/" target="_blank">siguiente enlace.</a>

14) Una vez finalizadas las modificaciones a nuestro archivo, vamos a proceder a subirlo, no sin antes agregar la info de nuestro nombre y correo al git local:

```
git config --global user.name "Tu nombre de cuenta de Gitlab"
git config --global user.email "Tu dirección de correo que asignaste a Gitlab"
```

Luego de estos pasos, vamos a escribir estas 3 sentencias para subir las modificaciones a nuestro repositorio:

```
git add .
git commit -m "Mi primera modificación a mi repositorio"
git push origin main
```

Con estos pasos, volviendo al navegador y refrescando la pantalla con F5 podremos ver las modificaciones al texto que realizamos.


### OPCIONAL: persistir clave en git


Cuando realicen varias modificaciones a sus repositorios, notarán si estos se encuentran en privado que resulta tedioso estar poniendo el nombre de usuario y clave cada vez que queramos hacer un cambio. Para evitarlo podemos "grabar" nuestra clave en un archivo y que no nos la pida, se la siguiente manera:

```
git config --global credential.helper store
git pull
```


